package com.hattarna.merahatt;

import com.hattarna.simulation.MainPanel;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

public class HattActivity extends Activity {

	Bitmap mBitmap=null;
	Bitmap mBitmaptwo=null;
	int ACTION_TAKE_VIDEO=1;
	VideoView mVideoView;
	Uri mVideoUri;
	Bitmap sample;
	String timeStamp;
	private Preview mPreview;
	private DrawOnTop mDrawOnTop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//huhuh
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Create the Preview view and set it as the content of the DancingQueen
        // Create the drawOnTop variable of the drawOnTop class that will
        // be responsible to acquiring the information from the screen and drawing it on a bitmap
        
        mDrawOnTop = new DrawOnTop(this);
        mPreview = new Preview(this, mDrawOnTop);
        setContentView(mPreview);
        addContentView(mDrawOnTop, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
        MainPanel panel = new MainPanel(this, mDrawOnTop);
		//addContentView(panel, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	}

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_hatt, menu);
		return true;
	}


}
