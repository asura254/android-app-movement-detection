package com.hattarna.merahatt;

import com.hattarna.math.Vec2;

public class IdentifiedNode {
	
	public Vec2 vector;
	public int id = -1;
	
	public IdentifiedNode(Vec2 vec, int id){
		this.vector = vec;
		this.id = id;
	}
	
	public IdentifiedNode(Vec2 vec){
		this.vector = vec;
	}
	
	// Calculates and returns the distance between two nodes
//	public double getDistance(Node previous, Node current){
//		return Math.sqrt((Math.pow((previous.vector.x-current.vector.x),2) + Math.pow((previous.vector.y-current.vector.y),2)));
//	}
}
