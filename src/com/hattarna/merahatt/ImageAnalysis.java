package com.hattarna.merahatt;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.widget.ImageView;

import com.hattarna.math.Vec2;


public class ImageAnalysis {
	Bitmap mBitmap=null;
	
	int mImageWidth;
	int mImageHeight;
	
	int[][] map;
	int[] pix;
	
	private ArrayList<ArrayList<Vec2>> clusters;
	private int currentCluster;
	
	public ArrayList<Vec2> nodes;

	public ImageAnalysis(Bitmap Image){
		this.mBitmap=Image;
	}

	public void run(){
		mImageWidth = mBitmap.getWidth();
		mImageHeight = mBitmap.getHeight();

		Log.d("MessageWidth","The Width is: "+mImageWidth);
		Log.d("MessageHeight","The Height is: "+mImageHeight);

		//The mapping for coordinates for Vec2
		map = new int[mImageWidth][mImageHeight];
		Log.d("MessageThree","Trying to load pixels");
		//A array with all of the pixels
		pix = new int[mImageWidth * mImageHeight];
		mBitmap.getPixels(pix, 0, mImageWidth, 0, 0, mImageWidth, mImageHeight);
		Log.d("MessageFour","Managed to load pixels");
		//SystemClock.sleep(1000);
		
		currentCluster = 0;
		clusters = new ArrayList<ArrayList<Vec2>>();
		
		for (int n = 0; n < pix.length; n++) {
			int x = n % mImageWidth;
			int y = n / mImageWidth;
			
			addCluster(new Vec2(x, y));
		}
		
		nodes = new ArrayList<Vec2>();
		for (int n = 0; n < clusters.size(); n++) {
			Vec2 nodeOffset = new Vec2();
			
			for (int i = 0; i < clusters.get(n).size(); i++) {
				nodeOffset.addSelf(clusters.get(n).get(i));
			}
			
			nodeOffset.scaleSelf(1 / (double) clusters.get(n).size());
			
			nodes.add(nodeOffset);
		}
	}
	
	private void addCluster(Vec2 pixel) {
		ArrayList<Vec2> queue = new ArrayList<Vec2>();
		
		if (addToQueue(pixel, queue)) {
			currentCluster++;
			
			ArrayList<Vec2> pixels = new ArrayList<Vec2>();
			
			clusters.add(pixels);
			
			while(queue.size() > 0) {
				Vec2 currentPixel = queue.get(0);
									queue.remove(0);
									
				pixels.add(currentPixel);
				
				addToQueue(new Vec2(currentPixel.x + 1, currentPixel.y), queue);
				addToQueue(new Vec2(currentPixel.x - 1, currentPixel.y), queue);
				
				addToQueue(new Vec2(currentPixel.x, currentPixel.y + 1), queue);
				addToQueue(new Vec2(currentPixel.x, currentPixel.y - 1), queue);
			}
		}
	}
	
	private boolean addToQueue(Vec2 pixel, ArrayList<Vec2> queue) {
		if (0 <= pixel.x && pixel.x < mImageWidth && 0 <= pixel.y && pixel.y < mImageHeight) {
			if(map[(int)pixel.x][(int)pixel.y] == 0) {
				int n = twoToOne(pixel);
				int red = Color.red(pix[n]);
				int blue = Color.blue(pix[n]);
				int green = Color.green(pix[n]);
				
				if(red > 240 && blue > 240 && green > 240){
					map[(int)pixel.x][(int)pixel.y] = currentCluster;
					queue.add(pixel);
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	private int twoToOne(Vec2 pixel) {
		return (int)((pixel.y)*mImageWidth + (pixel.x));
	}
}

