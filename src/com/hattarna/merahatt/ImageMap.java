package com.hattarna.merahatt;

import java.util.ArrayList;

import com.hattarna.math.Vec2;

public class ImageMap {
	
	public ArrayList<Vec2> vectorList;
	public ArrayList<IdentifiedNode> nodeList = new ArrayList<IdentifiedNode>();
	
	// For the first analysis
	public ImageMap(ArrayList<Vec2> vecList){
		this.vectorList = vecList;
		setNodes(vectorList);
	}
	
	// For the following times, when data from previous ImageMap is available
	public ImageMap(ArrayList<Vec2> vecList, ArrayList<IdentifiedNode> previousNodes){
		this.vectorList = vecList;
		setNodes(vectorList, previousNodes);
	}
	
	private void setNodes(ArrayList<Vec2> vectors) {
		for (int i = 0; i < vectors.size(); i++) {
			nodeList.add(new IdentifiedNode(vectors.get(i), i));
		}
	}
	
	private void setNodes(ArrayList<Vec2> newVectors, ArrayList<IdentifiedNode> previousNodes) {
		// Initialize distance-matrix
		double [][] distMap = new double[previousNodes.size()][newVectors.size()];
		// Create rows-list
		ArrayList<Integer> rows = new ArrayList<Integer>();
		for (int i = 0; i < previousNodes.size(); i++) {
			rows.add(i);
		}
		// Create columns-list
		ArrayList<Integer> columns = new ArrayList<Integer>();
		for (int i = 0; i < newVectors.size(); i++) {
			columns.add(i);
		}
		// Run through old and new values and calculate distances
		for (int i = 0; i < previousNodes.size(); i++) {
			for (int j = 0; j < newVectors.size(); j++) {
				if(i <= newVectors.size()) {
					distMap[i][j] = getDistance(previousNodes.get(i).vector, newVectors.get(j));
				} else if(i > newVectors.size()){
					distMap[i][j] = Double.MAX_VALUE;
				}
			}
		}
		// Runs through all values in the distance-matrix and makes sure that several new Nodes 
		// can't be mapped to the same id by deleting the rows and columns of those
		while(rows.size() > 0 && columns.size() > 0) {
			double smallest = Double.MAX_VALUE;
			int indexRow = -1;
			int indexCol = -1;
			for (int k = 0; k < rows.size(); k++) {
				for (int j = 0; j < columns.size(); j++) {
					if(distMap[rows.get(k)][columns.get(j)] < smallest){
						smallest = distMap[rows.get(k)][columns.get(j)];
						indexRow = rows.get(k);
						indexCol = columns.get(j);
					}
				}
			}
			rows.remove(rows.indexOf(indexRow));
			columns.remove(columns.indexOf(indexCol));
			
			if(indexRow != -1 && indexCol != -1) {
				nodeList.add(new IdentifiedNode(newVectors.get(indexCol), previousNodes.get(indexRow).id));
			}
		}
		// If the new vector has fewer values than the previous one, use the old values for those IDs
		if(rows.size()!=0){
			for (int j = 0; j < rows.size(); j++) {
				nodeList.add(new IdentifiedNode(previousNodes.get(rows.get(j)).vector, previousNodes.get(rows.get(j)).id));
			}
		}
	}
	
	public double getDistance(Vec2 vector1, Vec2 vector2){
		return Math.sqrt((Math.pow((vector1.x-vector2.x),2) + Math.pow((vector1.y-vector2.y),2)));
	}
}