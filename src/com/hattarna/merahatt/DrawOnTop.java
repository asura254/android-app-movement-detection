package com.hattarna.merahatt;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;

public class DrawOnTop extends View {
	//variables from the drawOnTOp
	public Bitmap mBitmap;
	Paint mPaintBlack;
	Paint mPaintYellow;
	Paint mPaintRed;
	Paint mPaintGreen;
	Paint mPaintBlue;
	byte[] mYUVData;
	int[] mRGBData;
	int mImageWidth, mImageHeight;
	//just used this as a parameter to check if the function was working
	int Abba=0;
	double[] mBinSquared;
	
	protected Bitmap drawable;
	protected Bitmap showable;

    public DrawOnTop(Context context) {
        super(context);
        
        // Init
     	drawable = Bitmap.createBitmap(480, 800, Bitmap.Config.ARGB_8888);
     	showable = Bitmap.createBitmap(480, 800, Bitmap.Config.ARGB_8888);
        
        mPaintBlack = new Paint();
        mPaintBlack.setStyle(Paint.Style.FILL);
        mPaintBlack.setColor(Color.BLACK);
        mPaintBlack.setTextSize(25);
        
        mPaintYellow = new Paint();
        mPaintYellow.setStyle(Paint.Style.FILL);
        mPaintYellow.setColor(Color.YELLOW);
        mPaintYellow.setTextSize(25);
        
        mPaintRed = new Paint();
        mPaintRed.setStyle(Paint.Style.FILL);
        mPaintRed.setColor(Color.RED);
        mPaintRed.setTextSize(25);
        
        mPaintGreen = new Paint();
        mPaintGreen.setStyle(Paint.Style.FILL);
        mPaintGreen.setColor(Color.GREEN);
        mPaintGreen.setTextSize(40);
        
        mPaintBlue = new Paint();
        mPaintBlue.setStyle(Paint.Style.FILL);
        mPaintBlue.setColor(Color.BLUE);
        mPaintBlue.setTextSize(25);
        
        mBitmap = null;
        mYUVData = null;
        mRGBData = null;
     
        mBinSquared = new double[256];
        for (int bin = 0; bin < 256; bin++)
        {
        	mBinSquared[bin] = ((double)bin) * bin;
        } // bin
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (mBitmap != null)
        {
        	Log.d("Render", "Once more onto the bridge");
        	
        	int canvasWidth = canvas.getWidth();
        	int canvasHeight = canvas.getHeight();
        	int newImageWidth = canvasWidth;
        	int newImageHeight = canvasHeight;
        	int marginWidth = (canvasWidth - newImageWidth)/2;
        	        	
        	// Convert from YUV to RGB
        	decodeYUV420SP(mRGBData, mYUVData, mImageWidth, mImageHeight);
        	//used this to set the draw framerate to 1/5 of the framerate
        		//from the camera, to check if it was indeed working
        	// Draw bitmap
        	mBitmap.setPixels(mRGBData, 0, mImageWidth, 0, 0, 
        			mImageWidth, mImageHeight);
        	Rect src = new Rect(0, 0, mImageWidth, mImageHeight);
        	Rect dst = new Rect(marginWidth, 0, 
        			canvasWidth-marginWidth, canvasHeight);
        	//just delete this next line in order to stop drawing the bitmaps on the screen
        	//canvas.drawBitmap(mBitmap, src, dst, mPaintBlack);
        	
        	// Draw black borders        	        	
//        	canvas.drawRect(0, 0, marginWidth, canvasHeight, mPaintBlack);
//        	canvas.drawRect(canvasWidth - marginWidth, 0, 
//        			canvasWidth, canvasHeight, mPaintBlack);
        	
        	
        	String imageStdDevStr = (new Integer(mImageWidth)).toString();
        	canvas.drawText(imageStdDevStr,newImageWidth/2-8, newImageHeight/2, mPaintGreen);
        	
        	
        	canvas.drawBitmap(showable, 0, 0, mPaintBlack);
        } 
        
        super.onDraw(canvas);
        
    } // end onDraw method
//function to decode all the data acquired from the screen in order to be able to use it in a bmp
    static public void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {
    	final int frameSize = width * height;
    	
    	for (int j = 0, yp = 0; j < height; j++) {
    		int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
    		for (int i = 0; i < width; i++, yp++) {
    			int y = (0xff & ((int) yuv420sp[yp])) - 16;
    			if (y < 0) y = 0;
    			if ((i & 1) == 0) {
    				v = (0xff & yuv420sp[uvp++]) - 128;
    				u = (0xff & yuv420sp[uvp++]) - 128;
    			}
    			
    			int y1192 = 1192 * y;
    			int r = (y1192 + 1634 * v);
    			int g = (y1192 - 833 * v - 400 * u);
    			int b = (y1192 + 2066 * u);
    			
    			if (r < 0) r = 0; else if (r > 262143) r = 262143;
    			if (g < 0) g = 0; else if (g > 262143) g = 262143;
    			if (b < 0) b = 0; else if (b > 262143) b = 262143;
    			
    			rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
    		}
    	}
    }
    
    static public void decodeYUV420SPGrayscale(int[] rgb, byte[] yuv420sp, int width, int height)
    {
    	final int frameSize = width * height;
    	
    	for (int pix = 0; pix < frameSize; pix++)
    	{
    		int pixVal = (0xff & ((int) yuv420sp[pix])) - 16;
    		if (pixVal < 0) pixVal = 0;
    		if (pixVal > 255) pixVal = 255;
    		rgb[pix] = 0xff000000 | (pixVal << 16) | (pixVal << 8) | pixVal;
    	} // pix
    }
    
    public void swap() {
		Bitmap temp = drawable;
		
		drawable = showable;
		showable = temp;
		
		drawable.eraseColor(0xFFFFFFFF);
	}
	
	public Bitmap getDrawable() {
		return drawable;
	}
} 
