package com.hattarna.simulation;

public abstract class Logic {
	public abstract void update(double deltaTime);
}
