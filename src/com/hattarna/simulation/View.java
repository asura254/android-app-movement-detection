package com.hattarna.simulation;

import android.graphics.Bitmap;

import com.hattarna.math.Vec2;

public class View {
	
	protected Vec2 offset;
	
	public View(Vec2 offset) {
		this.offset = offset;
	}
	
	public void render(Bitmap bitmap) {
		
		int radi = 2;
		
		for (int x = (int)offset.x - radi; x <= (int)offset.x + radi; x++) {
			for (int y = (int)offset.y - radi; y <= (int)offset.y + radi; y++) {
				if (0 <= x && x < 480 && 0 <= y && y < 800) {
					bitmap.setPixel(x, y, 0xFF000000);
				}
			}
		}
	}
}
