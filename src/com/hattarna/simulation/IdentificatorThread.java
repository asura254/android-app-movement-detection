package com.hattarna.simulation;

import java.util.ArrayList;
import com.hattarna.merahatt.DrawOnTop;

import android.graphics.Bitmap;
import android.util.Log;

import com.hattarna.math.Vec2;
import com.hattarna.merahatt.IdentifiedNode;
import com.hattarna.merahatt.ImageAnalysis;
import com.hattarna.merahatt.ImageMap;
import com.hattarna.utils.Timer;

public class IdentificatorThread extends Thread implements BufferedIdentifiedNodeCapturer {

	private boolean isUpdated;
	
	DrawOnTop drawOnTop;
	
	private ArrayList<IdentifiedNode> bufferedList;
	private ArrayList<IdentifiedNode> nodeList;
	
	public boolean running;
	
	public IdentificatorThread(DrawOnTop drawOnTop) {
		
		this.drawOnTop = drawOnTop;
		
		// Init
		running = true;
		
		isUpdated = false;
		bufferedList = null;
		
		// Start data
		
		ArrayList<Vec2> clusterList = new ArrayList<Vec2>();
		clusterList.add(new Vec2(240, 300));
		
		ImageMap map = new ImageMap(clusterList);
		
		nodeList = map.nodeList;
		
		setDone();
	}
	
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		while (running) {
			
			// Camera
			Bitmap mBitmap = null;
			
			while (mBitmap == null) {
				mBitmap = drawOnTop.mBitmap;
				
				//Log.d("AHAHA", "asdasdsad");
			}
			
			// Image thing
			ImageAnalysis imageA = new ImageAnalysis(mBitmap);
			imageA.run();
			
			for (int n = 0; n < imageA.nodes.size(); n++) {
				Log.d("ist", "ID: " +  imageA.nodes.get(n).toString());
			}
			
			ImageMap map = new ImageMap(imageA.nodes, bufferedList);
			
			nodeList = map.nodeList;
			
			for (int n = 0; n < nodeList.size(); n++) {
				Log.d("Node", "ID: " + nodeList.get(n).id + " Vector: " + nodeList.get(n).vector.toString());
			}
			
			setDone();
		}
	}
	
	private void setDone() {
		bufferedList = nodeList;
		
		isUpdated = true;
	}

	@Override
	public boolean getUpdated() {
		return isUpdated;
	}

	@Override
	public ArrayList<IdentifiedNode> captureData() {
		isUpdated = false;
		
		return bufferedList;
	}
}
