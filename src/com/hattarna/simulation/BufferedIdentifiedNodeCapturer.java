package com.hattarna.simulation;

import java.util.ArrayList;

import com.hattarna.merahatt.IdentifiedNode;
import com.hattarna.utils.BufferedCapturer;

public interface BufferedIdentifiedNodeCapturer extends BufferedCapturer<ArrayList<IdentifiedNode>> {}
