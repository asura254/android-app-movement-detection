package com.hattarna.simulation;

import com.hattarna.math.Vec2;

public class ConstraintAnchor extends Constraint {

	private Node A;
	private Vec2 anchor;
	
	private double distance;

	public ConstraintAnchor(Node A, Vec2 anchor, double distance) {
		this.A = A;
		
		this.anchor = anchor;
		this.distance = distance;
	}
	
	public ConstraintAnchor(Node A, Vec2 anchor) {
		this(A, anchor, 0);
	}


	@Override
	public void solve(double deltaTime) {
		Vec2 dist = anchor.sub(A.getOffset());

		double length = dist.length();

		if (length != 0)
			dist.scaleSelf(1 / length);

		double error = length - distance;
		
		A.getOffset().addSelf(dist.scale(error));
	}
}
