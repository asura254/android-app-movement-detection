package com.hattarna.simulation;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

import com.hattarna.math.cMath;
import com.hattarna.merahatt.DrawOnTop;

import com.hattarna.utils.Timer;

public class SimulationThread extends Thread {
	
	// Render
	private DrawOnTop panel;
	
	// Timer
	private Timer timer;
	private double deltaTime;
	private double maxDeltaTime;
	
	private boolean running;
	
	// Simulation
	private int iterationCount = 10;
	
	private ArrayList<Logic> logics;
	private ArrayList<Node> nodes;
	private ArrayList<Constraint> constraints;
	private ArrayList<View> views;
	
	public SimulationThread(DrawOnTop panel, BufferedIdentifiedNodeCapturer capturer) {
		
		this.panel = panel;
		
		// Init
		timer = new Timer();
		deltaTime = 0;
		maxDeltaTime = 1000;
		
		running = true;
		
		// Simulation
		logics = new ArrayList<Logic>();
		nodes = new ArrayList<Node>();
		constraints = new ArrayList<Constraint>();
		views = new ArrayList<View>();
		
		FactorySimulationBodies.createConstrainedBody(logics, nodes, constraints, views, capturer);
	}
	
	public void run() {
		while (running) {
			
			setDeltaTime();
			
			// Logic
			for (Logic logic : logics) {
				logic.update(deltaTime);
			}
			
			// Integrate
			for (Node node : nodes) {
				node.integrate(deltaTime);
			}
			
			// Reset integration
			for (Node node : nodes) {
				node.reset();
			}
			
			// Solve constraints
			for (int n = 0; n < iterationCount; n++) {
				for (Constraint constraint : constraints) {
					constraint.solve(deltaTime);
				}
			}
			
			// Render
			for (View view: views) {
				view.render(panel.getDrawable());
			}
			Log.d("Sim", "And here it is");
			// Blit to canvas
			panel.swap();
			/*Canvas canvas = null;
			
			//Canvas canvas = null;
			SurfaceHolder holder = panel.getHolder();
			
			try {
				canvas = holder.lockCanvas();
				
				synchronized (holder) {
					panel.onDraw(canvas);
				}
			}
			finally {
				if (canvas != null) {
					holder.unlockCanvasAndPost(canvas);
				}
			}*/
		}
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	protected void setDeltaTime() {
		deltaTime = cMath.cap(timer.getTime() / Math.pow(10, 9), maxDeltaTime);
		
		timer.reset();
	}
}
