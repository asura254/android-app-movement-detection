package com.hattarna.simulation;

public class BodyTypeHuman {
	public static int HEAD = 0;
	public static int UPPER_BODY = 1;
	public static int LOWER_BODY = 5;
	
	public static int RIGHT_ELBOW = 10;
	public static int RIGHT_HAND = 15;
	
	public static int LEFT_ELBOW = 20;
	public static int LEFT_HAND = 25;
	
	public static int RIGHT_KNEE = 30;
	public static int RIGHT_FOOT = 35;
	
	public static int LEFT_KNEE = 40;
	public static int LEFT_FOOT = 45;
}
