package com.hattarna.simulation;

public abstract class Constraint {
	public abstract void solve(double deltaTime);
}
