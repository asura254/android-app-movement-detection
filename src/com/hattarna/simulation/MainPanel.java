package com.hattarna.simulation;

import com.hattarna.merahatt.DrawOnTop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class MainPanel extends SurfaceView implements SurfaceHolder.Callback {
	
	protected Bitmap drawable;
	protected Bitmap showable;
	
	protected IdentificatorThread identificatorThread;
	protected SimulationThread simulationThread;
	
	protected Paint paint;
	
	public MainPanel(Context context, DrawOnTop drawOnTop) {
		super(context);
		
		getHolder().addCallback(this);
		
		// Init
		drawable = Bitmap.createBitmap(480, 800, Bitmap.Config.ARGB_8888);
		showable = Bitmap.createBitmap(480, 800, Bitmap.Config.ARGB_8888);
		
		identificatorThread = new IdentificatorThread(drawOnTop);
		simulationThread = new SimulationThread(drawOnTop, identificatorThread);
		
		paint = new Paint();
		
		paint.setColor(Color.BLACK); 
		paint.setTextSize(10); 
		
		setFocusable(true);
		
		identificatorThread.running = true;
		identificatorThread.start();
		
		simulationThread.setRunning(true);
		simulationThread.start();
		
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		
	}

	public void surfaceCreated(SurfaceHolder holder) {
		
	}
	
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		
		simulationThread.setRunning(false);
		identificatorThread.running = false;
		
		while(retry) {
			try {
				identificatorThread.join();
				
				retry = false;
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		retry = true;
		while(retry) {
			try {
				simulationThread.join();
				
				retry = false;
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		canvas.drawBitmap(showable, 0, 0, paint);
	}
	
	public void swap() {
		Bitmap temp = drawable;
		
		drawable = showable;
		showable = temp;
		
		drawable.eraseColor(0xFFFFFFFF);
	}
	
	public Bitmap getDrawable() {
		return drawable;
	}
}
