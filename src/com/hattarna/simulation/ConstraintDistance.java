package com.hattarna.simulation;

import android.util.Log;

import com.hattarna.math.Vec2;

public class ConstraintDistance extends Constraint {
	
	protected Node A;
	protected Node B;
	
	protected double distance;
	
	public ConstraintDistance(Node A, Node B, double distance) {
		this.A = A;
		this.B = B;
		
		this.distance = distance;
	}

	@Override
	public void solve(double deltaTime) {
		Vec2 dist = B.getOffset().sub(A.getOffset());
		
		double length = dist.length();
		
		if (length != 0)
			dist.scaleSelf(1 / length);
		
		double error = length - distance;
		
		double totalMass = A.getMass() + B.getMass();
		
		A.getOffset().addSelf(dist.scale( error * B.getMass() / totalMass));
		B.getOffset().addSelf(dist.scale(-error * A.getMass() / totalMass));
	}
}
