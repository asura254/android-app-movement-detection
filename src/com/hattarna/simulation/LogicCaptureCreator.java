package com.hattarna.simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.hattarna.math.Vec2;
import com.hattarna.merahatt.IdentifiedNode;

public class LogicCaptureCreator extends LogicCapture {
	private Map<Integer, Node> bodyMap;
	private Map<Integer, Constraint> linkMap;
	private Map<Integer, Vec2> anchorMap;
	private Map<Integer, View> viewMap;
	
	public LogicCaptureCreator(ArrayList<Logic> logics, ArrayList<Node> nodes, ArrayList<Constraint> constraints, ArrayList<View> views, BufferedIdentifiedNodeCapturer capturer) {
		super(logics, nodes, constraints, views, capturer);
		
		bodyMap = new HashMap<Integer, Node>();
		linkMap = new HashMap<Integer, Constraint>();
		anchorMap = new HashMap<Integer, Vec2>();
		viewMap = new HashMap<Integer, View>();
	}

	@Override
	public void update(double deltaTime) {
		if (capturer.getUpdated()) {
			ArrayList<IdentifiedNode> identifiedNodes = capturer.captureData();
			
			for (IdentifiedNode identifiedNode : identifiedNodes) {
				int id = identifiedNode.id;
				Vec2 bodyOffset = identifiedNode.vector.clone();
				Vec2 linkOffset = bodyOffset.clone();
				
				// Create new bodies if needed
				createBody(id, bodyOffset);
				createLink(id, linkOffset);
				createAnchor(id, linkOffset);
				createView(id, bodyOffset);
				
				// Update constraint
				updateConstraint(id, linkOffset);
			}
		}
	}
	
	private void updateConstraint(int id, Vec2 offset) {
		anchorMap.get(id).set(offset);
	}
	
	// Create new bodies
	private void createBody(int id, Vec2 offset) {
		if (!bodyMap.containsKey(id)) {
			Node node = new Node(offset, 1);
			
			bodyMap.put(id, node);
			
			nodes.add(node);
		}
	}
	
	private void createLink(int id, Vec2 offset) {
		if (!linkMap.containsKey(id)) {
			Constraint constraint = new ConstraintAnchor(bodyMap.get(id), offset);
			
			linkMap.put(id, constraint);
			
			constraints.add(constraint);
		}
	}
	
	private void createAnchor(int id, Vec2 offset) {
		if (!anchorMap.containsKey(id)) {
			anchorMap.put(id, offset);
		}
	}
	
	private void createView(int id, Vec2 offset) {
		if (!viewMap.containsKey(id)) {
			View view = new View(offset);
			
			viewMap.put(id, view);
			
			views.add(view);
		}
	}
}
