package com.hattarna.simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.hattarna.math.Vec2;

public class FactorySimulationBodies {
	
	public static void createDirectBody(ArrayList<Logic> logics, ArrayList<Node> nodes, ArrayList<Constraint> constraints, ArrayList<View> views, BufferedIdentifiedNodeCapturer capturer) {
		logics.add(new LogicCaptureCreator(logics, nodes, constraints, views, capturer));
	}
	
	public static void createConstrainedBody(ArrayList<Logic> logics, ArrayList<Node> nodes, ArrayList<Constraint> constraints, ArrayList<View> views, BufferedIdentifiedNodeCapturer capturer) {
		Vec2 start = new Vec2(240, 300);
		
		double mass = 1;
		
		Node head = new Node(start.add(new Vec2()), mass);
		Node upperBody = new Node(start.add(new Vec2(0, 10)), mass);
		Node lowerBody = new Node(start.add(new Vec2(0, 60)), mass);

		Node rightElbow = new Node(start.add(new Vec2(30, 10)), mass);
		Node rightHand = new Node(start.add(new Vec2(60, 10)), mass);
		
		Node leftElbow = new Node(start.add(new Vec2(-30, 10)), mass);
		Node leftHand = new Node(start.add(new Vec2(-60, 10)), mass);
		
		Node rightKnee = new Node(start.add(new Vec2(30, 60)), mass);
		Node rightFoot = new Node(start.add(new Vec2(60, 60)), mass);
		
		Node leftKnee = new Node(start.add(new Vec2(-30, 60)), mass);
		Node leftFoot = new Node(start.add(new Vec2(-60, 60)), mass);
		
		nodes.add(head);
		nodes.add(upperBody);
		nodes.add(lowerBody);
		
		nodes.add(rightElbow);
		nodes.add(rightHand);
		
		nodes.add(leftElbow);
		nodes.add(leftHand);
		
		nodes.add(rightKnee);
		nodes.add(rightFoot);
		
		nodes.add(leftKnee);
		nodes.add(leftFoot);
		
		Map<Integer, Node> bodyMap = new HashMap<Integer, Node>();
		
		bodyMap.put(BodyTypeHuman.HEAD, head);
		bodyMap.put(BodyTypeHuman.UPPER_BODY, upperBody);
		bodyMap.put(BodyTypeHuman.LOWER_BODY, lowerBody);
		
		bodyMap.put(BodyTypeHuman.RIGHT_ELBOW, rightElbow);
		bodyMap.put(BodyTypeHuman.RIGHT_HAND, rightHand);
		
		bodyMap.put(BodyTypeHuman.LEFT_ELBOW, leftElbow);
		bodyMap.put(BodyTypeHuman.LEFT_HAND, leftHand);
		
		bodyMap.put(BodyTypeHuman.RIGHT_KNEE, rightKnee);
		bodyMap.put(BodyTypeHuman.RIGHT_FOOT, leftFoot);
		
		bodyMap.put(BodyTypeHuman.LEFT_KNEE, leftKnee);
		bodyMap.put(BodyTypeHuman.LEFT_FOOT, leftFoot);
		
		
		Constraint neck = new ConstraintDistance(head, upperBody, head.getOffset().getDistance(upperBody.getOffset()));
		Constraint body = new ConstraintDistance(upperBody, lowerBody, upperBody.getOffset().getDistance(lowerBody.getOffset()));
		
		Constraint rightUpperArm = new ConstraintDistance(upperBody, rightElbow, upperBody.getOffset().getDistance(rightElbow.getOffset()));
		Constraint rightLowerArm = new ConstraintDistance(rightElbow, rightHand, rightElbow.getOffset().getDistance(rightHand.getOffset()));
		
		Constraint leftUpperArm = new ConstraintDistance(upperBody, leftElbow, upperBody.getOffset().getDistance(leftElbow.getOffset()));
		Constraint leftLowerArm = new ConstraintDistance(leftElbow, leftHand, leftElbow.getOffset().getDistance(leftHand.getOffset()));
		
		Constraint rightUpperLeg = new ConstraintDistance(lowerBody, rightKnee, lowerBody.getOffset().getDistance(rightKnee.getOffset()));
		Constraint rightLowerLeg = new ConstraintDistance(rightKnee, rightFoot, rightKnee.getOffset().getDistance(rightFoot.getOffset()));
		
		Constraint leftUpperLeg = new ConstraintDistance(lowerBody, leftKnee, lowerBody.getOffset().getDistance(leftKnee.getOffset()));
		Constraint leftLowerLeg = new ConstraintDistance(leftKnee, leftFoot, leftKnee.getOffset().getDistance(leftFoot.getOffset()));
		
		constraints.add(neck);
		constraints.add(body);
		
		constraints.add(rightUpperArm);
		constraints.add(rightLowerArm);
		
		constraints.add(leftUpperArm);
		constraints.add(leftLowerArm);
		
		constraints.add(rightUpperLeg);
		constraints.add(rightLowerLeg);
		
		constraints.add(leftUpperLeg);
		constraints.add(leftLowerLeg);
		
		views.add(new View(head.getOffset()));
		views.add(new View(upperBody.getOffset()));
		views.add(new View(lowerBody.getOffset()));
		
		views.add(new View(rightElbow.getOffset()));
		views.add(new View(rightHand.getOffset()));
		
		views.add(new View(leftElbow.getOffset()));
		views.add(new View(leftHand.getOffset()));
		
		views.add(new View(rightKnee.getOffset()));
		views.add(new View(rightFoot.getOffset()));
		
		views.add(new View(leftKnee.getOffset()));
		views.add(new View(leftFoot.getOffset()));
		
		logics.add(new LogicCaptureConstraint(logics, nodes, constraints, views, capturer, bodyMap));
	}
}
