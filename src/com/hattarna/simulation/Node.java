package com.hattarna.simulation;

import com.hattarna.math.Vec2;

public class Node {
	
	protected Vec2 oldOffset;
	protected Vec2 offset;
	
	protected Vec2 acceleration;
	
	protected double mass;
	
	public Node(Vec2 offset, double mass) {
		this.offset = offset;
		this.mass = mass;
		
		// Init
		this.oldOffset = offset.clone();
		
		this.acceleration = new Vec2();
	}
	
	public void integrate(double deltaTime) {
		offset.addSelf(offset.sub(oldOffset).scale(deltaTime).add(acceleration.scale(deltaTime * deltaTime / 2)));
	}
	
	public void reset() {
		oldOffset.set(offset);
		
		acceleration.set(0, 0);
	}
	
	// Get
	public Vec2 getOffset() {
		return offset;
	}
	
	public Vec2 getAcceleration() {
		return acceleration;
	}
	
	public double getMass() {
		return mass;
	}
}
