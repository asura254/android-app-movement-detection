package com.hattarna.simulation;

import java.util.ArrayList;

public abstract class LogicCapture extends Logic {
	
	protected BufferedIdentifiedNodeCapturer capturer;

	// Simulation
	protected ArrayList<Logic> logics;
	protected ArrayList<Node> nodes;
	protected ArrayList<Constraint> constraints;
	protected ArrayList<View> views;
	
	public LogicCapture(ArrayList<Logic> logics, ArrayList<Node> nodes, ArrayList<Constraint> constraints, ArrayList<View> views, BufferedIdentifiedNodeCapturer capturer) {
		this.logics = logics;
		this.nodes = nodes;
		this.constraints = constraints;
		this.views = views;
		
		this.capturer = capturer;
	}
	
	public abstract void update(double deltaTime);
}
