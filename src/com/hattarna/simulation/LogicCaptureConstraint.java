package com.hattarna.simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.hattarna.math.Vec2;
import com.hattarna.merahatt.IdentifiedNode;

public class LogicCaptureConstraint extends LogicCapture {
	
	private Map<Integer, Node> bodyMap;
	private Map<Integer, Constraint> linkMap;
	private Map<Integer, Vec2> anchorMap;

	public LogicCaptureConstraint(ArrayList<Logic> logics,ArrayList<Node> nodes, ArrayList<Constraint> constraints,ArrayList<View> views, BufferedIdentifiedNodeCapturer capturer, Map<Integer, Node> bodyMap) {
		super(logics, nodes, constraints, views, capturer);
		
		this.bodyMap = bodyMap;
		
		// Init
		linkMap = new HashMap<Integer, Constraint>();
		anchorMap = new HashMap<Integer, Vec2>();
	}

	@Override
	public void update(double deltaTime) {
		if (capturer.getUpdated() && capturer.captureData() != null) {
			ArrayList<IdentifiedNode> identifiedNodes = capturer.captureData();
			
			for (IdentifiedNode identifiedNode : identifiedNodes) {
				int id = identifiedNode.id;
				Vec2 bodyOffset = identifiedNode.vector.clone();
				Vec2 linkOffset = bodyOffset.clone();
				
				if (bodyMap.containsKey(id)) {
					// Create new bodies if needed
					createLink(id, linkOffset);
					createAnchor(id, linkOffset);
					
					// Update constraint
					updateConstraint(id, linkOffset);
				}
			}
		}
	}
	

	private void updateConstraint(int id, Vec2 offset) {
		anchorMap.get(id).set(offset);
	}
	
	private void createLink(int id, Vec2 offset) {
		if (!linkMap.containsKey(id)) {
			Constraint constraint = new ConstraintAnchor(bodyMap.get(id), offset);
			
			linkMap.put(id, constraint);
			
			constraints.add(constraint);
		}
	}
	
	private void createAnchor(int id, Vec2 offset) {
		if (!anchorMap.containsKey(id)) {
			anchorMap.put(id, offset);
		}
	}
}
