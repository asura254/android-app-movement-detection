package com.hattarna.utils;

public interface BufferedCapturer<T> {
	/**
	 * Returns true if the data recently got updated
	 * 
	 * @return If the data recently got updated
	 */
	public boolean getUpdated();
	
	/**
	 * Captures the data
	 * 
	 * Important to set isUpdated variable
	 * to false here.
	 * 
	 * @return 
	 */
	public T captureData();
}
