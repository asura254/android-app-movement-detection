package com.hattarna.utils;

public class Timer {

	protected long startTime;
	
	public Timer() {
		reset();
	}

	public long getTime() {
		return System.nanoTime() - startTime;
	}
	
	public void reset() {
		startTime = System.nanoTime();
	}
}
