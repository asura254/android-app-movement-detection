package com.hattarna.math;


public class cMath {

	// Limit
	public static double limit(double val, double max, double min) {
		if (val > max)
			return max;
		if (val < min)
			return min;
		return val;
	}
	
	public static double limit(double val, double max) {
		return limit(val, max, 0);
	}
	
	public static int limit(int val, int max, int min) {
		if (val > max)
			return max;
		if (val < min)
			return min;
		return val;
	}
	
	public static int limit(int val, int max) {
		return limit(val, max, 0);
	}
	
	// Narrow
	public static double narrow(double val, double limit) {
		if (val > limit)
			return limit;
		if (val < -limit)
			return -limit;
		return val;
	}
	
	public static int narrow(int val, int limit) {
		if (val > limit)
			return limit;
		if (val < -limit)
			return -limit;
		return val;
	}
	
	
	// Restrict
	public static double restrict(double val, double min) {
		if (val < min)
			return min;
		return val;
	}
	
	public static int restrict(int val, int min) {
		if (val < min)
			return min;
		return val;
	}
	
	// Cap
	public static double cap(double val, double max) {
		if (val > max)
			return max;
		return val;
	}
	
	public static int cap(int val, int max) {
		if (val > max)
			return max;
		return val;
	}
	
	// Random
	public static double random(double max, double min) {
		return Math.random() * (max - min) + min;
	}
	
	public static double random(int max, int min) {
		return Math.random() * (max - min) + min;
	}
	
	// Valley
	public static double randomValley(double limit, double offset) {
		return (2 * Math.random() - 1) * limit + offset;
	}
	
	public static double randomValley(int limit, int offset) {
		return (2 * Math.random() - 1) * limit + offset;
	}
	
	// Random integer
	public static int randomInteger(int max, int min) {
		return (int)(Math.random() * (max - min) + min);
	}
	
	public static int randomValleyInteger(int limit, int offset) {
		return (int)((2 * Math.random() - 1) * limit + offset);
	}
	
}
