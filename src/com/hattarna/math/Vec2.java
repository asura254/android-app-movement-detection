package com.hattarna.math;

public class Vec2 {

	public double x;
	public double y;
	
	public Vec2(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Vec2(double x) {
		this(x, 0);
	}
	
	public Vec2() {
		this(0, 0);
	}
	
	
	// Math
	public Vec2 clone() {
		return new Vec2(x, y);
	}
	
	public Vec2 set(double x, double y) {
		this.x = x;
		this.y = y;
		
		return this;
	}
	
	public Vec2 set(Vec2 A) {
		x = A.x;
		y = A.y;
		
		return this;
	}
	
	public Vec2 round() {
		return new Vec2(Math.round(x), Math.round(y));
	}
	public Vec2 roundSelf() {
		x = Math.round(x);
		y = Math.round(y);
		
		return this;
	}
	
	public Vec2 add(Vec2 A) {
		return new Vec2(x + A.x, y + A.y);
	}
	public Vec2 addSelf(Vec2 A) {
		x += A.x;
		y += A.y;
		
		return this;
	}
	
	public Vec2 sub(Vec2 A) {
		return new Vec2(x - A.x, y - A.y);
	}
	public Vec2 subSelf(Vec2 A) {
		x -= A.x;
		y -= A.y;
		
		return this;
	}
	
	public Vec2 scale(double scale) {
		return new Vec2(x * scale, y * scale);
	}
	public Vec2 scaleSelf(double scale) {
		x *= scale;
		y *= scale;
		
		return this;
	}
	
	public double dot(Vec2 A) {
		return x * A.x + y * A.y;
	}
	
	public double length() {
		return Math.sqrt(x * x + y * y);
	}
	
	public Vec2 normalize() {
		double length = this.length();
		
		if (length != 0)
			return new Vec2(x / length, y / length);
		return new Vec2();
	}
	public Vec2 normalizeSelf() {
		double length = this.length();
		
		if (length == 0)
			return this;
		
		x /= length;
		y /= length;
		
		return this;
	}
	
	public double getDistance(Vec2 A) {
		return this.sub(A).length();
	}
	
	public String toString() {
		return "{ " + Double.toString(x) + ", " + Double.toString(y) + " }";
	}
}
